import { match } from "assert";
import { fileURLToPath } from "url";

function almostIncreasingSequence(sequence: number[]): boolean {
  let count: number = 0;
  for (let i = 1; i < sequence.length; i++) {
    if (sequence[i] <= sequence[i - 1]) {
      if (++count > 1) {
        return false;
      }

      if (
        i >= 2 &&
        i < sequence.length - 1 &&
        sequence[i] <= sequence[i - 2] &&
        sequence[i + 1] <= sequence[i - 1]
      ) {
        return false;
      }
    }
  }

  return true;
}

/*
Given matrix, a rectangular matrix of integers, where each value represents the cost of the room,
your task is to return the total sum of all rooms that are suitable for the CodeBots
(ie: add up all the values that don't appear below a 0).

Example
For

matrix = [[0, 1, 1, 2],
          [0, 5, 0, 0],
          [2, 0, 3, 3]]
the output should be
matrixElementsSum(matrix) = 9

1 + 1 + 2 + 5
*/
const matrixElements = (matrix: number[][]): number => {
  let sum: number = 0,
    val: number;

  //Rest of the array
  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[0].length; j++) {
      if ((val = matrix[i][j]) === 0 && i !== matrix.length - 1) {
        matrix[i + 1][j] = 0;
      }

      sum += val;
    }
  }
  return sum;
};

/* Given an array of strings, return another array containing all of its longest strings.

Example

For inputArray = ["aba", "aa", "ad", "vcd", "aba"], the output should be
allLongestStrings(inputArray) = ["aba", "vcd", "aba"]. */

function allLongestStrings(inputArray: string[]): string[] {
  let longestStrings: string[] = [];
  let maxStringLength: number = 0;

  for (const str of inputArray) {
    if (longestStrings.length === 0) {
      longestStrings.push(str);
      maxStringLength = str.length;
    } else if (str.length === maxStringLength) {
      longestStrings.push(str);
    } else if (str.length > maxStringLength) {
      longestStrings = [str];
      maxStringLength = str.length;
    }
  }

  return longestStrings;
}

function countOccurences(str: string): { [char: string]: number } {
  const occurences: { [char: string]: number } = { };

  for (const char of str) {
    if (occurences.hasOwnProperty(char)) {
      occurences[char] += 1;
    } else {
      occurences[char] = 1;
    }
  }

  return occurences;
}
function commonCharacterCount(s1: string, s2: string): number {
  const occurences_s1: { [char: string]: number } = countOccurences(s1);
  const occurences_s2: { [char: string]: number } = countOccurences(s2);

  let count: number = 0;

  for (const char in occurences_s1) {
    if (occurences_s2.hasOwnProperty(char)) {
      count += Math.min(occurences_s1[char], occurences_s2[char]);
    }
  }

  return count;
}

/* 
Ticket numbers usually consist of an even number of digits.
A ticket number is considered lucky  if the sum of the first half of the digits is equal
to the sum of the second half.*/

const isLucky = (n: number): boolean => {
  const n_str: string = n.toString(10);
  const firstHalf: string = n_str.substring(0, n_str.length / 2);
  const secondHalf: string = n_str.substring(n_str.length / 2);

  const sumFirstHalf = firstHalf
    .split("")
    .map(Number)
    .reduce((a, b) => a + b);
  const sumSecondHalf = secondHalf
    .split("")
    .map(Number)
    .reduce((a, b) => a + b);

  return sumFirstHalf === sumSecondHalf;
};

/*
Your task is to rearrange the people by their heights in a non-descending order without moving the trees. People can be very tall!

Example

For a = [-1, 150, 190, 170, -1, -1, 160, 180], the output should be
sortByHeight(a) = [-1, 150, 160, 170, -1, -1, 180, 190].
*/

const customSortFunction = (a: number, b: number) => {
  let returnValue: number = a === -1 || b === -1 || a <= b ? 0 : a - b;
  console.log(`${a} vs ${b} -> ${returnValue}`);
  return returnValue;
};
const sortByHeight = (a: number[]): number[] => {
  const tree: number[] = [];
  const person: number[] = [];
  let val: number;

  for (let i = 0; i < a.length; i++) {
    if ((val = a[i]) === -1) {
      tree.push(i);
    } else {
      person.push(val);
      person.sort((x, y) => x - y);
    }
  }

  for (const j of tree) {
    person.splice(j, 0, -1);
  }

  return person;
};

/*
Write a function that reverses characters in (possibly nested) parentheses in the input string.

Input strings will always be well-formed with matching ()s.

Example

For inputString = "(bar)", the output should be
reverseInParentheses(inputString) = "rab";

For inputString = "foo(bar)baz", the output should be
reverseInParentheses(inputString) = "foorabbaz";

For inputString = "foo(bar)baz(blim)", the output should be
reverseInParentheses(inputString) = "foorabbazmilb";

For inputString = "foo(bar(baz))blim", the output should be
reverseInParentheses(inputString) = "foobazrabblim".

Because "foo(bar(baz))blim" becomes "foo(barzab)blim" and then "foobazrabblim"
*/

const reverseString = (str: string): string => {
  return str.split("").reverse().join("");
};

function reverseInParentheses(str: string): string {
  if (str === "()") {
    return "";
  }

  let newStr: string = str,
    leftParentheseIndex: number,
    rightParentheseIndex: number,
    substring: string,
    substringWithParenthses: string;

  while ((leftParentheseIndex = newStr.lastIndexOf("(")) !== -1) {
    rightParentheseIndex = newStr.indexOf(")", leftParentheseIndex);
    substringWithParenthses = newStr.substring(
      leftParentheseIndex,
      rightParentheseIndex + 1
    );
    substring = substringWithParenthses.substring(
      1,
      substringWithParenthses.length - 1
    );
    newStr = newStr.replace(substringWithParenthses, reverseString(substring));
  }

  return newStr;
}

function alternatingSums(a: number[]): number[] {
  let teamOne_sum: number = 0,
    teamTwo_sum: number = 0,
    activeTeam: number = 1;

  for (const value of a) {
    if (activeTeam === 1) {
      teamOne_sum += value;
      activeTeam = 2;
    } else {
      teamTwo_sum += value;
      activeTeam = 1;
    }
  }

  return [teamOne_sum, teamTwo_sum];
}

function addBorder(picture: string[]): string[] {
  const borderLength: number = picture[0].length;
  const borderedPicture: string[] = ["*".repeat(borderLength + 2)];

  for (const str of picture) {
    borderedPicture.push(`*${str}*`);
  }

  borderedPicture.push("*".repeat(borderLength + 2));
  return borderedPicture;
}

function areSimilar(a: number[], b: number[]): boolean {
  const swapIndexes: number[] = [];
  let nbSwaps: number = 0;

  for (let i = 0; i < a.length; i++) {
    if (a[i] !== b[i]) {
      if (++nbSwaps > 2) {
        return false;
      }

      swapIndexes.push(i);
    }
  }

  if (nbSwaps === 0) {
    return true;
  }

  const idx_1: number = swapIndexes[0];
  const idx_2: number = swapIndexes[1];

  return a[idx_1] === b[idx_2] && a[idx_2] === b[idx_1];
}

function arrayChange(arr: number[]): number {
  let nbChanges: number = 0,
    incrementValue: number;

  for (let i = 0; i < arr.length - 1; i++) {
    if (arr[i] >= arr[i + 1]) {
      incrementValue = arr[i] - arr[i + 1] + 1;
      nbChanges += incrementValue;
      arr[i + 1] += incrementValue;
    }
  }
  return nbChanges;
}

/*
For inputString = "aabb", the output should be
palindromeRearranging(inputString) = true.

We can rearrange "aabb" to make "abba", which is a palindrome.

Input/Output
*/

function palindromeRearranging(inputString: string): boolean {
  const str_arr = inputString.split("");
  let char: string,
    index: number,
    charThatOccursOnce: number = 0;

  while (str_arr.length > 0) {
    char = str_arr.shift();
    index = str_arr.indexOf(char);

    if (index === -1) {
      charThatOccursOnce++;

      if (charThatOccursOnce > 1) {
        return false;
      }
    } else {
      str_arr.splice(index, 1);
    }
  }

  return true;
}

function arrayMaximalAdjacentDifference(inputArray: number[]): number {
  let maximumDiff: number = Number.MIN_SAFE_INTEGER;

  for (let i = 0; i < inputArray.length - 1; i++) {
    maximumDiff = Math.max(
      maximumDiff,
      Math.abs(inputArray[i] - inputArray[i + 1])
    );
  }

  return maximumDiff;
}

function isNumeric(value: string): boolean {
  return /^-?\d+$/.test(value);
}

function isIPv4Address(inputString: string): boolean {
  const numbers: string[] = inputString.split(".");
  let n: number;

  if (numbers.length !== 4) {
    return false;
  }

  for (const item of numbers) {
    if (!isNumeric(item)) {
      return false;
    }

    n = parseInt(item, 10);

    if (n < 0 || n > 255) {
      return false;
    }
  }

  return true;
}

function avoidObstacles(inputArray: number[]): number {
  const max: number = Math.max(...inputArray);
  let divs: boolean;

  for (let i = 1; i < max; i++) {
    divs = inputArray.some((x) => x % i === 0);
    if (!divs) {
      return i;
    }
  }

  return max + 1;
}

function boxBlur(image: number[][]): number[][] {
  const box: number[][] = [];
  let temp: number[], sum: number;
  let i: number, j: number;

  const validPosition = (row: number, col: number): boolean => {
    return 0 <= row && row < image.length && 0 <= col && col < image[0].length;
  };

  const getSquareSum = (row: number, col: number): number => {
    let sum: number = 0;

    for (let i = row - 1; i <= row + 1; i++) {
      for (let j = col - 1; j <= col + 1; j++) {
        if (validPosition(i, j)) {
          sum += image[i][j];
        } else {
          return undefined;
        }
      }
    }
    return sum;
  };

  for (i = 0; i < image.length; i++) {
    temp = [];

    for (j = 0; j < image[0].length; j++) {
      if ((sum = getSquareSum(i, j)) !== undefined) {
        temp.push(Math.floor(sum / 9));
      }
    }

    if (temp.length > 0) {
      box.push(temp);
    }
  }

  return box;
}

function minesweeper(matrix: boolean[][]): number[][] {
  const grid: number[][] = Array.from(Array(matrix.length), (_) =>
    Array(matrix[0].length).fill(0)
  );

  const isValidPosition = (row: number, col: number): boolean => {
    return (
      0 <= row && row < matrix.length && 0 <= col && col < matrix[0].length
    );
  };

  const explodeMine = (row: number, col: number): void => {
    if (!matrix[row][col]) {
      return;
    }

    for (let i = row - 1; i <= row + 1; i++) {
      for (let j = col - 1; j <= col + 1; j++) {
        if (isValidPosition(i, j)) {
          grid[i][j]++;
        }
      }
    }

    if (matrix[row][col]) {
      grid[row][col]--;
    }

    return;
  };

  for (let i = 0; i < matrix.length; i++) {
    for (let j = 0; j < matrix[0].length; j++) {
      explodeMine(i, j);
    }
  }

  return grid;
}
function arrayReplace(
  inputArray: number[],
  elemToReplace: number,
  substitutionElem: number
): number[] {
  return inputArray.map((x) => {
    return x === elemToReplace ? substitutionElem : x;
  });
}

function evenDigitsOnly(n: number): boolean {
  const arr: number[] = n.toString(10).split("").map(Number);
  return arr.some((x) => x % 2 !== 0);
}

function variableName(name: string): boolean {
  return /^(?![\d])[a-zA-Z\d_]+$/gm.test(name);
}

function alphabeticShift(inputString: string): string {
  const shiftChar = (char: string): string => {
    let charCode: number = char.charCodeAt(0);
    const shiftedCharCode: number = ++charCode <= 122 ? charCode : 97;
    return String.fromCharCode(shiftedCharCode);
  };

  return inputString
    .split("")
    .map((char) => shiftChar(char))
    .join("");
}

const getColor = (cell: string): string => {
  const split = cell.split("");
  const charCode: number = split[0].charCodeAt(0);
  const number: number = parseInt(split[1], 10);

  let currentColor: string = charCode % 2 === 0 ? "white" : "black";

  for (let i = 2; i <= number; i++) {
    currentColor = currentColor === "white" ? "black" : "white";
  }

  return currentColor;
};

function chessBoardCellColor(cell1: string, cell2: string): boolean {
  return getColor(cell1) === getColor(cell2);
}

const wrapNumber = (
  n: number,
  increaseValue: number,
  start: number,
  limit: number
): number => {
  return start + ((n + increaseValue) % limit);
};

function circleOfNumbers(n: number, firstNumber: number): number {
  const arr: number[] = Array.from(Array(n).keys());
  const startingIndex: number = arr.indexOf(firstNumber);
  const index: number = wrapNumber(
    startingIndex,
    arr.length / 2,
    0,
    arr.length
  );
  return arr[index];
}
function depositProfit(
  deposit: number,
  rate: number,
  threshold: number
): number {
  let nbYears: number = 0;
  const p: number = 1 + rate / 100;

  while (deposit < threshold) {
    deposit *= p;
    nbYears++;
  }

  return nbYears;
}

function absoluteValuesSumMinimization(arr: number[]): number {
  const getAbsoluteValuesSum = (n: number): number => {
    let sum: number = 0;

    for (const x of arr) {
      sum += Math.abs(x - n);
    }

    return sum;
  };

  let minValue: number = Number.MAX_SAFE_INTEGER;
  let minSum: number = Number.MAX_SAFE_INTEGER;
  let sum: number;

  for (const x of arr) {
    if ((sum = getAbsoluteValuesSum(x)) <= minSum) {
      minValue = sum === minSum ? Math.min(minValue, x) : x;
      minSum = sum;
    }
  }

  return minValue;
}

const differByOneChar = (s1: string, s2: string): boolean => {
  const s1_arr: string[] = s1.split("");
  const s2_arr: string[] = s2.split("");
  let count: number = 0;

  for (let i = 0; i < s1.length; i++) {
    if (s1_arr[i] !== s2_arr[i]) {
      count++;

      if (count > 1) {
        return false;
      }
    }
  }

  return count === 1;
};

const getArrayPermutations = (arr: any[]): any[][] => {
  let permutations: any[][] = [];

  for (let i = 0; i < arr.length; i++) {
    let temp: any[][] = getArrayPermutations(
      arr.slice(0, i).concat(arr.slice(i + 1))
    );

    if (!temp.length) {
      permutations.push([arr[i]]);
    } else {
      for (let j = 0; j < temp.length; j = j + 1) {
        permutations.push([arr[i]].concat(temp[j]));
      }
    }
  }
  return permutations;
};
function stringsRearrangement(inputArray: string[]): boolean {
  let found: boolean = false;

  for (const perm of getArrayPermutations(inputArray)) {
    found = true;

    for (let i = 0; i < perm.length - 1; i++) {
      if (!differByOneChar(perm[i], perm[i + 1])) {
        found = false;
        break;
      }
    }

    if (found) {
      break;
    }
  }

  return found;
}

function extractEachKth(inputArray: number[], k: number): number[] {
  const getRemoveIndex = (): number[] => {
    const indexes: number[] = [];
    let n: number = 1;
    let index: number;

    while ((index = n * k - 1) < inputArray.length) {
      indexes.push(index);
      n++;
    }

    return indexes;
  };

  const result: number[] = [];
  const removedIndexes: number[] = getRemoveIndex();

  for (let i = 0; i < inputArray.length; i++) {
    if (!removedIndexes.includes(i)) {
      result.push(inputArray[i]);
    }
  }

  return result;
}

const firstDigit = (str: string): string => {
  return /(\d)/gm.exec(str)[0];
};

function differentSymbolsNaive(s: string): number {
  return Array.from(new Set(s.split(""))).length;
}

function arrayMaxConsecutiveSum(inputArray: number[], k: number): number {
  let maxSum: number = inputArray.slice(0, k).reduce((a, b) => a + b);
  let currentSum: number = maxSum;

  for (let i = k; i < inputArray.length; i++) {
    currentSum += inputArray[i] - inputArray[i - k];
    maxSum = Math.max(currentSum, maxSum);
  }

  return maxSum;
}

function growingPlant(
  upSpeed: number,
  downSpeed: number,
  desiredHeight: number
): number {
  let height: number = upSpeed;
  const dailyGrow: number = upSpeed - downSpeed;
  let day = 1;

  while (height < desiredHeight) {
    height += dailyGrow;
    day++;
  }

  return day;
}

function knapsackLight(
  value1: number,
  weight1: number,
  value2: number,
  weight2: number,
  maxW: number
): number {
  let maxValue: number = 0;
  const item_1: boolean = weight1 <= maxW;
  const item_2: boolean = weight2 <= maxW;

  // Can take both items
  if (weight1 + weight2 <= maxW) {
    maxValue = value1 + value2;
  }

  // If both items fit in the bag separately, choose the most valuable
  else if (item_1 && item_2) {
    maxValue = Math.max(value1, value2);
  }

  //Only item 1 can fit
  else if (item_1) {
    maxValue = value1;
  }

  //Only item 2 can fit
  else if (item_2) {
    maxValue = value2;
  }

  return maxValue;
}

function longestDigitsPrefix(inputString: string): string {
  const match: RegExpExecArray = /^(\d+)/gm.exec(inputString);
  return match ? match[0] : "";
}
const sumDigits = (x: number): number => {
  return x
    .toString(10)
    .split("")
    .map(Number)
    .reduce((a, b) => a + b);
};

function digitDegree(n: number): number {
  let degree: number = 0;

  while (n.toString(10).length > 1) {
    degree++;
    n = sumDigits(n);
  }

  return degree;
}

const getChessCoordinates = (coordinates: string): number[] => {
  const split: string[] = coordinates.split("");
  return [
    8 - parseInt(split[1], 10),
    split[0].toLowerCase().charCodeAt(0) - 97,
  ];
};

const areValidChessCoordinates = (numericCoordinates: number[]): boolean => {
  return !numericCoordinates.some((x) => x < 0 || x > 7);
};

// https://app.codesignal.com/arcade/intro/level-9/6M57rMTFB9MeDeSWo
function bishopAndPawn(bishop: string, pawn: string): boolean {
  const pawnCoordinates: number[] = getChessCoordinates(pawn);
  const bishopCoordinates: number[] = getChessCoordinates(bishop);
  const directions = ["upLeft", "upRight", "downLeft", "downRight"];

  const check = (direction: string): boolean => {
    let i: number = bishopCoordinates[0];
    let j: number = bishopCoordinates[1];

    while (areValidChessCoordinates([i, j])) {
      if (i === pawnCoordinates[0] && j === pawnCoordinates[1]) {
        return true;
      }

      if (direction === "upLeft") {
        i--;
        j--;
      } else if (direction === "downLeft") {
        i++;
        j--;
      } else if (direction === "upRight") {
        i--;
        j++;
      } else if (direction === "downRight") {
        i++;
        j++;
      } else {
        console.error(`Unknown direction: ${direction}`);
        break;
      }
    }

    return false;
  };

  for (const d of directions) {
    if (check(d)) {
      return true;
    }
  }

  return false;
}

// https://app.codesignal.com/arcade/intro/level-10/PHSQhLEw3K2CmhhXE

const wrap = (n: number, minBound: number, maxBound: number): number => {
  const range = maxBound - minBound + 1;
  n = (n - minBound) % range;
  if (n < 0) return maxBound + 1 + n;
  else return minBound + n;
};

const getPreviousLetter = (char: string): string => {
  const previousCharCode = wrap(char.charCodeAt(0) - 1, 97, 122);
  return String.fromCharCode(previousCharCode);
};

function isBeautifulString(inputString: string): boolean {
  const occurences = countOccurences(inputString);
  let previousChar: string;
  let hasPreviousLetter: boolean;

  for (const char in occurences) {
    if (char !== "a") {
      previousChar = getPreviousLetter(char);
      hasPreviousLetter = occurences.hasOwnProperty(previousChar);

      if (
        !hasPreviousLetter ||
        (hasPreviousLetter && occurences[previousChar] < occurences[char])
      ) {
        return false;
      }
    }
  }

  return true;
}

function findEmailDomain(address: string): string {
  const domainPattern: RegExp = /([a-z\d\-.]+.[a-z]+)$/gm;
  const match: RegExpExecArray = domainPattern.exec(address);
  return match ? match[0] : "";
}

const isPalindrome = (str: string): boolean => {
  return str === reverseString(str);
};

const getSuffix = (str: string, acc: string = ""): string => {
  if (isPalindrome(str)) {
    return reverseString(acc);
  }

  return getSuffix(str.substr(1), acc + str[0]);
};

/* Exemple:
 * abcdc -> suffix = reverseString(ab) = "ba"
 * palindrome -> abcdcba
 */

const buildPalindrome = (str: string): string => {
  return str + getSuffix(str);
};

function electionsWinners(votes: number[], k: number): number {
  const maxVotes: number = Math.max(...votes);

  if (k === 0) {
    return votes.indexOf(maxVotes) === votes.lastIndexOf(maxVotes) ? 1 : 0;
  }

  return votes.filter((nbVotes) => nbVotes + k > maxVotes).length;
}

function isMAC48Address(inputString: string): boolean {
  const MAC48_Pattern: RegExp = /^([0-9A-F]{2}-){5}([0-9A-F]{2})$/gm;
  return MAC48_Pattern.test(inputString);
}

function isDigit(symbol: string): boolean {
  const n = parseInt(symbol, 10);
  return !isNaN(n) && n < 10;
}

/* First, the string is divided into the least possible number of disjoint substrings consisting of identical characters
for example, "aabbbc" is divided into ["aa", "bbb", "c"]
Next, each substring with length greater than one is replaced with a concatenation of its length and the repeating character
for example, substring "bbb" is replaced by "3b"
Finally, all the new strings are concatenated together in the same order and a new string is returned.
Example

For s = "aabbbc", the output should be
lineEncoding(s) = "2a3bc". */

const customConcatStr = (str: string): string => {
  if (str.length === 0) {
    return "";
  }
  return str.length > 1 ? `${str.length}${str[0]}` : str[0];
};

function lineEncoding(str: string) {
  let accumulator: string = "";
  let encodedStr: string = "";

  for (let i = 0; i < str.length; i++) {
    if (accumulator.length === 0 || str[i] === accumulator[0]) {
      accumulator += str[i];
    } else if (str[i] !== accumulator[0]) {
      encodedStr += customConcatStr(accumulator);
      accumulator = str[i];
    }
  }

  if (accumulator.length > 0) {
    encodedStr += customConcatStr(accumulator);
  }

  return encodedStr;
}

function chessKnight(cell: string): number {
  const coordinates = getChessCoordinates(cell);
  const x = coordinates[0];
  const y = coordinates[1];

  const possiblePositions = [
    [x - 1, y + 2],
    [x - 2, y + 1],
    [x - 1, y - 2],
    [x - 2, y - 1],
    [x + 1, y - 2],
    [x + 2, y - 1],
    [x + 1, y + 2],
    [x + 2, y + 1],
  ];

  const validPositions = possiblePositions.filter((p) =>
    areValidChessCoordinates(p)
  );
  return validPositions.length;
}

function deleteDigit(n: number): number {
  let maxNumber: number = Number.MIN_SAFE_INTEGER;
  const digitSplit: string[] = n.toString(10).split("");
  let newNumber: number, temp: string[];

  for (let i = 0; i < digitSplit.length; i++) {
    temp = [...digitSplit];
    temp.splice(i, 1);
    newNumber = parseInt(temp.join(""));
    maxNumber = Math.max(maxNumber, newNumber);
  }

  return maxNumber;
}

function longestWord(str: string) {
  const pattern: RegExp = /([a-zA-Z]+)/gm;
  let matches: RegExpExecArray;
  let longestWord: string = "";

  while ((matches = pattern.exec(str)) !== null) {
    if (matches.index === pattern.lastIndex) {
      pattern.lastIndex++;
    }

    matches.forEach((match) => {
      if (match.length > longestWord.length) {
        longestWord = match;
      }
    });
  }

  return longestWord;
}

const isBetween = (
  x: number,
  lowerBound: number,
  upperBound: number
): boolean => {
  return lowerBound <= x && x <= upperBound;
};

function validTime(time: string): boolean {
  const timeArr: number[] = time.split(":").map(Number);
  return isBetween(timeArr[0], 0, 23) && isBetween(timeArr[1], 0, 59);
}

function sumUpNumbers(inputString: string): number {
  const pattern: RegExp = /(\d+)/gm;
  let matches: RegExpExecArray;
  let sum: number = 0;

  while ((matches = pattern.exec(inputString)) !== null) {
    if (matches.index === pattern.lastIndex) {
      pattern.lastIndex++;
    }

    matches.forEach((match) => {
      sum += parseInt(match, 10) / 2;
    });
  }

  return sum;
}

const arrayCompare = (arr1: number[], arr2: number[]): boolean => {
  if (arr1.length !== arr2.length) {
    return false;
  }

  if (arr1.length === 0) {
    return true;
  }

  if (arr1[0] !== arr2[0]) {
    return false;
  }

  return arrayCompare(arr1.slice(1), arr2.slice(1));
};

const squareEqual = (square1: number[][], square2: number[][]) => {
  return (
    arrayCompare(square1[0], square2[0]) && arrayCompare(square1[1], square2[1])
  );
};

function differentSquares(matrix: number[][]): number {
  let squares: number[][][] = [];
  let s: number[][];
  let firstRow: number[], secondRow: number[];

  for (let i = 0; i < matrix.length - 1; i++) {
    for (let j = 0; j < matrix[0].length; j++) {
      firstRow = matrix[i].slice(j, j + 2);
      secondRow = matrix[i + 1].slice(j, j + 2);

      if (firstRow.length === 2 && secondRow.length === 2) {
        s = [firstRow, secondRow];

        if (!squares.some((x) => squareEqual(x, s))) {
          squares.push(s);
        }
      }
    }
  }

  return squares.length;
}

function digitsProduct(product: number): number {
  if (product <= 9) {
    return product === 0 ? 10 : product;
  }

  let divisors: number[] = [];

  for (let i = 9; i >= 2 && product > 1; i--) {
    while (product % i === 0) {
      product /= i;
      divisors.push(i);
    }
  }

  if (product !== 1) {
    return -1;
  }

  divisors.sort((a, b) => a - b);
  return divisors.length > 0 ? parseInt(divisors.join("")) : -1;
}

const renameFile = (filename: string, k: number): string => {
  return `${filename}(${k})`;
};

function fileNaming(names: string[]): string[] {
  const filesList: string[] = [...names];

  let existingFiles: string[],
    filename: string,
    renameCount: number,
    renamedFile: string,
    renamed: boolean;

  for (let i = 0; i < filesList.length; i++) {
    filename = filesList[i];
    existingFiles = filesList.slice(0, i);

    if (existingFiles.includes(filename)) {
      renamed = false;
      renameCount = 0;

      while (!renamed) {
        renamedFile = renameFile(filename, ++renameCount);
        if (!existingFiles.includes(renamedFile)) {
          renamed = true;
          filesList[i] = renamedFile;
        }
      }
    }
  }

  return filesList;
}

function chunkSubstr(str: string, size: number): string[] {
  const numChunks: number = Math.ceil(str.length / size);
  const chunks: string[] = new Array(numChunks);

  for (let i = 0, o = 0; i < numChunks; ++i, o += size) {
    chunks[i] = str.substr(o, size);
  }

  return chunks;
}

function messageFromBinaryCode(code: string): string {
  return String.fromCharCode(
    ...chunkSubstr(code, 8).map((c) => parseInt(c, 2))
  );
}

function spiralNumbers(n: number): number[][] {
  const arr: number[][] = new Array(n).fill(0).map(() => new Array(n).fill(0));

  const areValidCoordinates = (i: number, j: number): boolean => {
    return isBetween(i, 0, n - 1) && isBetween(j, 0, n - 1);
  };

  const directions = ["right", "down", "left", "up"];

  let done: boolean = false,
    x: number = 0,
    directionIndex = 0,
    newDirectionFound: boolean,
    nbTries: number,
    i: number = 0,
    j: number = 0,
    next_i: number,
    next_j: number;

  while (!done) {
    arr[i][j] = ++x;

    newDirectionFound = false;
    nbTries = 1;

    while (!newDirectionFound && nbTries < 5) {
      switch (directions[directionIndex]) {
        case "right": {
          next_i = i;
          next_j = j + 1;
          break;
        }

        case "down": {
          next_i = i + 1;
          next_j = j;
          break;
        }

        case "left": {
          next_i = i;
          next_j = j - 1;
          break;
        }

        case "up": {
          next_i = i - 1;
          next_j = j;
          break;
        }

        default:
          console.error(`Unknown direction: ${directions[directionIndex]}`);
          return [];
      }

      if (areValidCoordinates(next_i, next_j) && arr[next_i][next_j] === 0) {
        i = next_i;
        j = next_j;
        newDirectionFound = true;
      } else {
        directionIndex = wrap(directionIndex + 1, 0, directions.length - 1);
        nbTries++;
      }
    }

    done = nbTries > 4;
  }

  return arr;
}

function sudoku(grid: number[][]): boolean {
  const validRow: number[] = Array.from(Array(9), (_, i) => i + 1);
  let temp: number[], i: number, j: number;

  //Check row
  for (const row of grid) {
    temp = [...row].sort((a, b) => a - b);
    if (!arrayCompare(validRow, temp)) {
      return false;
    }
  }

  //Check columns 
  for (i = 0; i < 9; i++) {
    temp = grid.map(row => row[i]).sort((a, b) => a - b);
    if (!arrayCompare(temp, validRow)) {
      return false;
    }
  }

  let firstRow: number[],
    secondRow: number[],
    thirdRow: number[],
    square: number[];

  for (let i = 0; i < 7; i += 3) {
    for (let j = 0; j < 7; j += 3) {
      firstRow = grid[i].slice(j, j + 3);
      secondRow = grid[i + 1].slice(j, j + 3);
      thirdRow = grid[i + 2].slice(j, j + 3);

      square = firstRow.concat(secondRow, thirdRow);
      square.sort((a, b) => a - b);

      if (!arrayCompare(validRow, square)) {
        return false;
      }
    }
  }
  return true;
}
const grid = [[1, 3, 2, 5, 4, 6, 9, 8, 7],
[4, 6, 5, 8, 7, 9, 3, 2, 1],
[7, 9, 8, 2, 1, 3, 6, 5, 4],
[9, 2, 1, 4, 3, 5, 8, 7, 6],
[3, 5, 4, 7, 6, 8, 2, 1, 9],
[6, 8, 7, 1, 9, 2, 5, 4, 3],
[5, 7, 6, 9, 8, 1, 4, 3, 2],
[2, 4, 3, 6, 5, 7, 1, 9, 8],
[8, 1, 9, 3, 2, 4, 7, 6, 5]];

sudoku(grid);