const wrap = (n: number, minBound: number, maxBound: number): number => {
  const range = maxBound - minBound + 1;
  n = (n - minBound) % range;
  return n < 0 ? maxBound + 1 + n : minBound + n;
};

const init2DArray = (nbRows: number, nbCols: number): number[][] => {
  return Array.from(Array(nbRows), (_) => Array(nbCols).fill(0));
};

//Tableau de [1...n]
const initArray_1_N = (n: number): number[] => {
  return Array.from(Array(n), (_, i) => i + 1);
}

//Tableau de [0...n]
const initArray_0_N = (n: number): number[] => {
  return Array.from(Array(n).keys());
}

//Tableau de [start, end]
const arrayRange = (start: number, end: number): number[] => {
  return Array.from({ length: end + 1 - start }, (v, k) => k + start);
}

/* Supprime tous les éléments du tableau de l à r (inclus)
Exemple: removeArrayPart([1,2,3,4,5,6,7,8], 2, 4) -> [1, 2, 6, 7, 8]
*/
const removeArrayPart = (inputArray: number[], l: number, r: number): number[] => {
  const firstPart: number[] = inputArray.slice(0, l);
  const secondPart: number[] = inputArray.slice(r + 1, inputArray.length)
  return firstPart.concat(secondPart);
}

//Permutations 
function permute(input: any[]): any[][] {
  const ret: any[][] = [];

  for (let i = 0; i < input.length; i++) {
    const rest = permute(input.slice(0, i).concat(input.slice(i + 1)));

    if (!rest.length) {
      ret.push([input[i]]);
    } else {
      for (let j = 0; j < rest.length; j = j + 1) {
        ret.push([input[i]].concat(rest[j]));
      }
    }
  }
  return ret;
}

//Occurences
function countOccurences(str: string): { [char: string]: number } {
  const occurences: { [char: string]: number } = {};

  for (const char of str) {
    if (occurences.hasOwnProperty(char)) {
      occurences[char] += 1;
    } else {
      occurences[char] = 1;
    }
  }

  return occurences;
}