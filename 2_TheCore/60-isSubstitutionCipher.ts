const getCharAlphabetIndex = (char: string): number => {
  return char.toLowerCase().charCodeAt(0) - 96;
};

const checkCipher = (s1: string, s2: string): boolean => {
  const cypher: number[] = new Array(27).fill(0);
  let result: boolean = true,
    s1_index: number,
    s2_index: number;

  for (let i = 0; i < s1.length; i++) {
    s1_index = getCharAlphabetIndex(s1[i]);
    s2_index = getCharAlphabetIndex(s2[i]);

    if (cypher[s1_index] === 0) {
      cypher[s1_index] = s2_index;
    } else if (cypher[s1_index] !== s2_index) {
      result = false;
      break;
    }

  }

  return result;
};

function isSubstitutionCipher(s1: string, s2: string) {
  return checkCipher(s1, s2) && checkCipher(s2, s1);
}