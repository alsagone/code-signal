const myAddition = (a: number, b: number): number => {
  return (a + b) % 10;
};

function additionWithoutCarrying(param1: number, param2: number): number {
  let a_str: string = param1.toString(10),
    b_str = param2.toString(10),
    n: number;

  //Pad the shortest string
  const lengthDifference: number = a_str.length - b_str.length;

  if (lengthDifference > 0) {
    b_str = "0".repeat(Math.abs(lengthDifference)) + b_str;
  } else if (lengthDifference < 0) {
    a_str = "0".repeat(Math.abs(lengthDifference)) + a_str;
  }

  const result: number[] = [];

  for (let i = 0; i < a_str.length; i++) {
    n = myAddition(parseInt(a_str[i]), parseInt(b_str[i]));
    result.push(n);
  }

  return parseInt(result.join(""));
}
