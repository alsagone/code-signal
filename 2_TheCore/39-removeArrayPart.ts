function removeArrayPart(inputArray: number[], l: number, r: number): number[] {
  const firstPart: number[] = inputArray.slice(0, l);
  const secondPart: number[] = inputArray.slice(r + 1, inputArray.length)
  return firstPart.concat(secondPart);
}