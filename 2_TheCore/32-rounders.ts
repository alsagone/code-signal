const round = (n: number): number => {
  if (n < 0 || n > 9) {
    throw `Incorrect number ${n}`;
  }

  return n >= 5 ? 10 : 0;
};

function rounders(n: number): number {
  const n_arr: number[] = n.toString(10).split("").map(Number);

  let rounded: boolean = false,
    index: number = n_arr.length,
    roundedNumber: number;

  while (!rounded) {
    rounded = true;

    for (let i = index - 1; i >= 1; i--) {
      if (n_arr[i] !== 0) {
        index = i;
        rounded = false;
        break;
      }
    }

    if (!rounded) {
      roundedNumber = round(n_arr[index]);

      if (roundedNumber === 10) {
        n_arr[index] = 0;
        n_arr[index - 1]++;
      } else {
        n_arr[index] = 0;
      }
    }
  }

  return parseInt(n_arr.join(""));
}

console.log(rounders(1234));
console.log(rounders(1445));
