/*
N candles are placed in a row, some of them are initially lit.
For each candle from the 1st to the Nth the following algorithm is applied:
if the observed candle is lit then states of this candle and all candles before it are changed to the opposite.
Which candles will remain lit after applying the algorithm to all candles in the order they are placed in the line? 
*/

const solution = (candles: number[]): number[] => {
  const lit: number = 1,
    out: number = 0;
  let i: number, j: number;

  for (i = 0; i < candles.length; i++) {
    if (candles[i] === lit) {
      for (j = 0; j <= i; j++) {
        candles[j] = candles[j] === out ? lit : out;
      }
    }
  }

  return candles;
};
