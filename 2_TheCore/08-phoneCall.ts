function phoneCall(min1: number, min2_10: number, min11: number, s: number): number {
  let callCost: number = 0, callDuration: number = 0;

  while (callCost <= s) {
    if (callDuration < 1) {
      callCost += min1;
    }

    else if (1 <= callDuration && callDuration < 10) {
      callCost += min2_10;
    }

    else if (callDuration >= 10) {
      callCost += min11;
    }

    callDuration++;
  }

  return callDuration - 1;
}

console.log(phoneCall(3, 1, 2, 20))