function killKthBit(n: number, k: number) {
  return n & ~(1 << (k - 1));
}
