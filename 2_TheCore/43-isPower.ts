/*
The idea is simple to try all numbers x starting from 2 to square root of n (given number).
For every x, try x^y where y starts from 2 and increases one by one until either x^y becomes n or greater than n. 
*/

function isPower(n: number): boolean {
  if (n <= 1) {
    return true;
  }

  let x: number, y: number, pow: number;

  for (x = 2; x <= Math.sqrt(n); x++) {
    y = 2;

    while (!((pow = Math.pow(x, y)) > n)) {
      if (pow === n) {
        return true;
      }

      y++;
    }
  }

  return false;
}

console.log(isPower(125));
