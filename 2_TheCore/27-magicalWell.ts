function magicalWell(a: number, b: number, n: number): number {
  let dollars: number = 0;

  while (n-- > 0) {
    dollars += a++ * b++;
  }

  return dollars;
}