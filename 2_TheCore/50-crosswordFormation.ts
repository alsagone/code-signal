function crosswordFormation(words: string[]) {
  return permute(words).reduce((acc, p) => acc + countSolutions(p), 0);
}

function countSolutions(words: string[]): number {
  let count: number = 0, j: number, l: number, n: number;

  for (let i = 0; i < words[0].length; i++) {
    j = words[1].indexOf(words[0][i]);

    while (j >= 0) {
      for (let k = i + 2; k < words[0].length; k++) {
        l = words[2].indexOf(words[0][k]);

        while (l >= 0) {
          for (let m = j + 2; m < words[1].length; m++) {
            n = words[3].indexOf(words[1][m]);
            while (n >= 0) {
              if (
                words[3].length - n > k - i &&
                words[2].length - l > m - j &&
                words[3][k - i + n] == words[2][m - j + l]
              ) {
                count++;
              }

              n = words[3].indexOf(words[1][m], n + 1);
            }
          }

          l = words[2].indexOf(words[0][k], l + 1);
        }
      }

      j = words[1].indexOf(words[0][i], j + 1);
    }
  }
  return count;
}

function permute(input: string[]): string[][] {
  const ret: string[][] = [];

  for (let i = 0; i < input.length; i++) {
    const rest = permute(input.slice(0, i).concat(input.slice(i + 1)));

    if (!rest.length) {
      ret.push([input[i]]);
    } else {
      for (let j = 0; j < rest.length; j = j + 1) {
        ret.push([input[i]].concat(rest[j]));
      }
    }
  }
  return ret;
}
