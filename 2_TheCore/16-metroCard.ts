const getDaysInMonth = (month: number): number => {
  return new Date(2021, month, 0).getDate();
};

function metroCard(lastNumberOfDays: number): number[] {
  const daysInMonth: number[] = Array.from(Array(12), (_, i) => i + 1).map(
    (n) => getDaysInMonth(n)
  );

  const set = new Set([]);

  for (let i = 0; i < daysInMonth.length - 1; i++) {
    if (daysInMonth[i] <= lastNumberOfDays) {
      set.add(daysInMonth[i + 1]);
    }
  }

  return Array.from(set).sort((a, b) => a - b);
}

console.log(metroCard(31))