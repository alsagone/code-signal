const getDigitSum = (n: number): number => {
  return n
    .toString(10)
    .split("")
    .map(Number)
    .reduce((a, b) => a + b);
};

function solution(n: number): number {
  const sequence: number[] = [];
  const digitsSum: number[] = [];
  let done: boolean = false,
    d: number;

  while (!done) {
    sequence.push(n);

    d = getDigitSum(n);
    digitsSum.push(d);

    if (n > 0) {
      n -= d;
    } else {
      done = true;
    }
  }

  const dict: { [key: number]: number } = {};

  for (const sum of digitsSum) {
    if (!dict.hasOwnProperty(sum)) {
      dict[sum] = 1;
    } else {
      dict[sum]++;
    }
  }

  let mostFrequentSum: number = 0,
    nbOccurences: number = 0;

  for (const sum of Object.keys(dict)) {
    if (dict[sum] > nbOccurences) {
      nbOccurences = dict[sum];
      mostFrequentSum = parseInt(sum, 10);
    } else if (dict[sum] === nbOccurences) {
      mostFrequentSum = Math.max(mostFrequentSum, parseInt(sum, 10));
    }
  }

  return mostFrequentSum;
}