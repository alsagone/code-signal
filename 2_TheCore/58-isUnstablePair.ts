/*To compare two filenames a and b, find the first position i at which a[i] ≠ b[i].
If a[i] < b[i], then a < b. Otherwise a > b.
If such position doesn't exist, the shorter string goes first.

For filename1 = "aa" and filename2 = "AAB", the output should be
isUnstablePair(filename1, filename2) = true.

Because "AAB" < "aa", but "AAB" > "AA"
*/

function isUnstablePair(f1: string, f2: string): boolean {
  return f1 < f2 !== f1.toLowerCase() < f2.toLowerCase();
}