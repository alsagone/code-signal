function largestNumber(n: number): number {
  return Math.pow(10, n) - 1;
}
