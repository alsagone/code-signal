function leastFactorial(n: number): number {
  let k: number = 0;
  let fact_k: number = 1;

  while (fact_k < n) {
    k++;
    fact_k *= k;
  }

  return fact_k;
}

console.log(leastFactorial(17));
