function appleBoxes(k: number): number {
  const applesCount: number[] = [0, 0];

  for (let i = 1; i <= k; i++) {
    applesCount[i % 2] += Math.pow(i, 2);
  }

  return applesCount[0] - applesCount[1];
}

console.log(appleBoxes(5));
