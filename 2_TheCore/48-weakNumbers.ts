const countDivisors = (n: number): number => {
  let total: number = 1,
    p: number,
    i: number,
    count: number;
  const hash: boolean[] = Array(n + 1).fill(true);

  for (p = 2; p * p < n; p++) {
    if (hash[p]) {
      for (i = p * 2; i < n; i += p) {
        hash[i] = false;
      }
    }
  }

  for (p = 2; p <= n; p++) {
    if (hash[p]) {
      count = 0;

      if (n % p === 0) {
        while (n % p === 0) {
          n = (n / p) >> 0;
          count++;
        }

        total *= count + 1;
      }
    }
  }

  return total;
};

const getWeaknesses = (limit: number): { [key: number]: number } => {
  const weaknesses_dict: { [key: number]: number } = {};
  const divisors_dict: { [key: number]: number } = {};
  let i: number, weakness: number, n: number;

  for (i = 1; i <= limit; i++) {
    divisors_dict[i] = countDivisors(i);
  }

  //We define the weakness of number x as the number of positive integers smaller than x that have more divisors than x.
  for (i = 1; i <= limit; i++) {
    weakness = 0;

    for (const key of Object.keys(divisors_dict)) {
      n = parseInt(key, 10);

      if (n < i && divisors_dict[n] > divisors_dict[i]) {
        weakness++;
      }
    }

    weaknesses_dict[i] = weakness;
  }

  return weaknesses_dict;
};

function weakNumbers(n: number): number[] {
  const weakness_dict: { [key: number]: number } = getWeaknesses(n);

  const weakness_list: number[] = Object.keys(weakness_dict).map(
    (key) => weakness_dict[key]
  );

  const maxWeakness: number = Math.max(...weakness_list);
  const weaknessCount: number = weakness_list.reduce(
    (a, v) => (v === maxWeakness ? a + 1 : a),
    0
  );
  return [maxWeakness, weaknessCount];
}

console.log(weakNumbers(9));
