const arrayCompare = (arr1: number[], arr2: number[]): boolean => {
  if (arr1.length !== arr2.length) {
    return false;
  }

  if (arr1.length === 0) {
    return true;
  }

  if (arr1[0] !== arr2[0]) {
    return false;
  }

  return arrayCompare(arr1.slice(1), arr2.slice(1));
};

const squareEqual = (square1: number[][], square2: number[][]) => {
  return (
    arrayCompare(square1[0], square2[0]) && arrayCompare(square1[1], square2[1])
  );
};

function solution(matrix: number[][]): number {
  let squares: number[][][] = [];
  let s: number[][];
  let firstRow: number[], secondRow: number[];

  for (let i = 0; i < matrix.length - 1; i++) {
    for (let j = 0; j < matrix[0].length; j++) {
      firstRow = matrix[i].slice(j, j + 2);
      secondRow = matrix[i + 1].slice(j, j + 2);

      if (firstRow.length === 2 && secondRow.length === 2) {
        s = [firstRow, secondRow];

        if (!squares.some((x) => squareEqual(x, s))) {
          squares.push(s);
        }
      }
    }
  }

  return squares.length;
}