function addTwoDigits(n: number): number {
  return n
    .toString(10)
    .split("")
    .map(Number)
    .reduce((a, b) => a + b);
}
