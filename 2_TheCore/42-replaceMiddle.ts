const replaceMiddle = (arr: number[]): number[] => {
  if (arr.length % 2 !== 0) {
    return arr;
  }

  const middleIndex: number = arr.length / 2;
  const middleSum: number = arr[middleIndex - 1] + arr[middleIndex];
  return arr
    .slice(0, middleIndex - 1)
    .concat(middleSum, arr.slice(middleIndex + 1));
};

const arr = [7, 2, 2, 5, 10, 7];
console.log(replaceMiddle(arr));
