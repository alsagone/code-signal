const arrayRange = (start: number, end: number): number[] => {
  return Array.from({ length: end + 1 - start }, (v, k) => k + start);
};

const isBetween = (x: number, min: number, max: number): boolean => {
  return min <= x && x <= max;
}

function countSumOfTwoRepresentations2(
  n: number,
  l: number,
  r: number
): number {
  const arr: number[] = arrayRange(l, r);
  const complements: number[] = arr.map((x) => n - x);

  let count: number = 0,
    value: number,
    complement: number;

  for (let i = 0; i < complements.length; i++) {
    value = arr[i];
    complement = complements[i];

    if (value <= complement && isBetween(complement, l, r)) {
      count++;
    }
  }

  return count;
}

console.log(countSumOfTwoRepresentations2(6, 2, 4));
