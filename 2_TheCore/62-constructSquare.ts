function getDigitsCount(str) {
  var d = [];
  while (str.length) {
    var c = str[0];
    d.push(str.length - (str = str.replace(new RegExp(c, "g"), "")).length);
  }
  return d.sort((a, b) => b - a).join("");
}

console.log(getDigitsCount("11222"));