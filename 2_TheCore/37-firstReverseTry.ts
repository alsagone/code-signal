function firstReverseTry(arr: number[]): number[] {
  if (arr.length < 2) {
    return arr;
  }

  const temp: number = arr[0];
  arr[0] = arr[arr.length - 1];
  arr[arr.length - 1] = temp;
  return arr;
}
