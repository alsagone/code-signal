/*
Let's call two integers A and B friends if each integer from the array divisors is either a divisor of both A and B or neither A nor B.
If two integers are friends, they are said to be in the same clan.
How many clans are the integers from 1 to k, inclusive, broken into? 
*/

const numberOfClans = (divisors: number[], k: number) => {
  const clans: number[] = Array(1024).fill(0);
  let c: number, i: number, j: number;

  for (i = 1; i <= k; i++) {
    c = 0;
    for (j = 0; j < divisors.length; j++) {
      if (i % divisors[j] === 0) {
        c = c | (1 << j);
      }
    }
    clans[c] = 1;
  }
  return clans.reduce((a, b) => a + b);
}
