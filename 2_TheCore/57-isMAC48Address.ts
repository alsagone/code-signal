function isMAC48Address(inputString: string): boolean {
  const MAC48_Pattern: RegExp = /^([0-9A-F]{2}-){5}([0-9A-F]{2})$/gm;
  return MAC48_Pattern.test(inputString);
}