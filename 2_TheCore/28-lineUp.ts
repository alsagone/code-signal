const wrap = (n: number, minBound: number, maxBound: number): number => {
  const range = maxBound - minBound + 1;
  n = (n - minBound) % range;
  return n < 0 ? maxBound + 1 + n : minBound + n;
};

function lineUp(commands: string): number {
  if (commands.length === 0) {
    return 0;
  }

  const directions: string[] = ["facing", "left", "back", "right"];

  const getNewDirection = (currentDirection: string, command: string): string => {
    const index = directions.indexOf(currentDirection);

    if (index === -1) {
      console.error(`Unknown direction: ${currentDirection}`);
      return currentDirection;
    }

    let increment: number;

    switch (command) {
      case "L":
        increment = 1;
        break;

      case "R":
        increment = -1;
        break;

      case "A":
        increment = 2;
        break;

      default:
        console.error(`Unknown command: ${command}`);
        increment = 0;
        break;
    }

    const newIndex: number = wrap(index + increment, 0, 3);
    return directions[newIndex];
  };

  const moveConfused = (currentDirection: string, command: string): string => {
    let confusedPerson: string;

    switch (command) {
      case "L":
        confusedPerson = getNewDirection(currentDirection, "R");
        break;

      case "R":
        confusedPerson = getNewDirection(currentDirection, "L");
        break;

      default:
        confusedPerson = getNewDirection(currentDirection, command);
        break;
    }

    return confusedPerson;
  };

  let normalPerson: string = getNewDirection("facing", commands[0]),
    confusedPerson: string = moveConfused("facing", commands[0]),
    sameDirectionCount: number = normalPerson === confusedPerson ? 1 : 0;

  for (let i = 1; i < commands.length; i++) {
    normalPerson = getNewDirection(normalPerson, commands[i]);
    confusedPerson = moveConfused(confusedPerson, commands[i]);
    if (normalPerson === confusedPerson) {
      sameDirectionCount++;
    }
  }

  return sameDirectionCount;
}
