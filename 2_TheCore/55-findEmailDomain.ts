function findEmailDomain(address: string): string {
  const regex: RegExp = /@([a-zA-Z0-9.-]+.[a-zA-Z0-9.-]+)$/gm;
  const matches: RegExpExecArray = regex.exec(address);
  return matches[1];
}
