const getAlphabetIndex = (char: string): number => {
  return char.toLowerCase().charCodeAt(0) - 96;
};

const solution = (str: string): boolean => {
  let b: boolean = true;

  const indexesArr: number[] = str
    .split("")
    .map((char) => getAlphabetIndex(char));

  for (let i = 0; i < indexesArr.length - 1; i++) {
    if (indexesArr[i] >= indexesArr[i + 1]) {
      b = false;
      break;
    }
  }

  return b;
};

console.log(solution("bcz"));
console.log(solution("effg"));
