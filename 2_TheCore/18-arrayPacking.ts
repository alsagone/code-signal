const padBinary = (binStr: string): string => {
  return binStr.length < 8 ? "0".repeat(8 - binStr.length) + binStr : binStr;
};

function arrayPacking(a: number[]): number {
  const arr: string[] = a.map((n) => padBinary(n.toString(2))).reverse();
  const bigBinNumber: string = arr.join("");
  return parseInt(bigBinNumber, 2);
}

console.log(arrayPacking([24, 85, 0]));
