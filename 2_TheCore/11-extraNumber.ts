function countOccurences(arr: number[]): { [key: number]: number } {
  const occurences: { [key: number]: number } = { };

  for (const n of arr) {
    if (occurences.hasOwnProperty(n)) {
      occurences[n] += 1;
    } else {
      occurences[n] = 1;
    }
  }

  return occurences;
}

function extraNumber(a: number, b: number, c: number): number {
  const occurences = countOccurences([a, b, c]);
  let extraNumber: number;

  for (const n in occurences) {
    if (occurences[n] === 1) {
      extraNumber = parseInt(n);
      break;
    }
  }

  return extraNumber;
}
