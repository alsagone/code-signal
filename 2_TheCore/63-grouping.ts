const getGroup = (n: number): number => {
  return Math.ceil(n / Math.pow(10, 4));
}

function solution(arr: number[]): number {
  const dict: { [key: number]: number[] } = {};
  let nbGroups: number = 0, group: number;

  for (const n of arr) {
    group = getGroup(n);

    if (!dict.hasOwnProperty(group)) {
      dict[group] = [n];
      nbGroups++;
    }

    else {
      dict[group].push(n);
    }
  }

  return arr.length + nbGroups;
}