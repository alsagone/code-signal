function addMinutes(date: Date, minutes: number): Date {
  return new Date(date.getTime() + minutes * 60000);
}

function sumOfDigits(n: number): number {
  return n
    .toString(10)
    .split("")
    .map(Number)
    .reduce((a, b) => a + b);
}

function lateRide(n: number): number {
  const newDate: Date = addMinutes(new Date("December 17, 1995 00:00:00"), n);
  return sumOfDigits(newDate.getHours()) + sumOfDigits(newDate.getMinutes());
}

console.log(lateRide(30));
