function tennisSet(score1: number, score2: number): boolean {
  const scores: number[] = [score1, score2].sort((a, b) => (a > b ? -1 : 1));
  const scoreGap: number = scores[0] - scores[1];
  return (
    (scores[0] === 6 && scoreGap > 1) ||
    (scores[0] === 7 && (scoreGap === 1 || scoreGap === 2))
  );
}
