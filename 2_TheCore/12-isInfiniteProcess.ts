function isInfiniteProcess(a: number, b: number): boolean {
  let gap: number, newGap: number;
  while (a !== b) {
    gap = Math.abs(a - b);
    newGap = Math.abs(++a - --b);

    if (newGap > gap) {
      return true;
    }
  }

  return false;
}

/*
while a is not equal to b do
  increase a by 1
  decrease b by 1
*/
