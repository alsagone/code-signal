const getRoundness = (n: number): number => {
  const regex: RegExp = /([0]+)$/gm;
  const m: RegExpExecArray = regex.exec(n.toString(10));
  return m !== null ? m[0].length : 0;
};

function increaseNumberRoundness(n: number): boolean {
  const roundness: number = getRoundness(n);
  const n_str: string = n.toString(10);

  if (roundness === 0) {
    return n_str.includes("0");
  }

  //Remove the trailing zeroes of the number
  const trimmed_n: string = n_str.slice(0, n_str.length - roundness);

  //If there are still zeroes in the trimmed part, we can increase the roundness
  return trimmed_n.includes("0");
}