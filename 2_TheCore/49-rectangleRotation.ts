function rectangleRotation(a: number, b: number): number {
  const ld: number = a / Math.sqrt(2) / 2;
  const sd: number = b / Math.sqrt(2) / 2;
  const rectExt: number[] = [2 * Math.floor(ld) + 1, 2 * Math.floor(sd) + 1];
  const rectInt: number[] = [
    2 * Math.floor(ld) + (ld % 1 < 0.5 ? 0 : 2),
    2 * Math.floor(sd) + (sd % 1 < 0.5 ? 0 : 2),
  ];
  return rectExt[0] * rectExt[1] + rectInt[0] * rectInt[1];
}
