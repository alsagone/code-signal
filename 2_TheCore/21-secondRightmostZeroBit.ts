function secondRightmostZeroBit(n: number): number {
  return ~(n | n + 1) & (n | n + 1) + 1;
}
