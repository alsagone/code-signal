const sumSquaredDigits = (n: number): number => {
  return n
    .toString(10)
    .split("")
    .map((x) => Math.pow(parseInt(x, 10), 2))
    .reduce((a, b) => a + b);
};

function squareDigitsSequence(a0: number): number {
  const queue: number[] = [a0];
  let squaredDigitsSum: number,
    done: boolean = false;

  while (!done) {
    squaredDigitsSum = sumSquaredDigits(queue.slice(-1)[0]);
    done = queue.includes(squaredDigitsSum);
    queue.push(squaredDigitsSum);
  }

  return queue.length;
}

console.log(squareDigitsSequence(16));
