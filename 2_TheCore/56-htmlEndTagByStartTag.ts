function htmlEndTagByStartTag(startTag: string): string {
  const start: number = startTag.indexOf("<") + 1;
  const end: number = startTag.includes(" ")
    ? startTag.indexOf(" ")
    : startTag.indexOf(">");

  return `</${startTag.slice(start, end)}>`;
}

["<button type='button' disabled>", "<i>"].forEach((s) => {
  console.log(htmlEndTagByStartTag(s));
});
