function seatsInTheater(nCols: number, nRows: number, col: number, row: number): number {
  const rowsBlocked = nRows - row;
  const colsBlocked = nCols - col + 1;
  return rowsBlocked * colsBlocked;
}
