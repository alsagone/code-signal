function countOccurences(str: string): { [char: string]: number } {
  const occurences: { [char: string]: number } = {};

  for (const char of str) {
    if (occurences.hasOwnProperty(char)) {
      occurences[char] += 1;
    } else {
      occurences[char] = 1;
    }
  }

  return occurences;
}

function stringsConstruction(a: string, b: string): number {
  const occurences_a: { [char: string]: number } = countOccurences(a);
  const occurences_b: { [char: string]: number } = countOccurences(b);

  let count: number = Number.MAX_SAFE_INTEGER;

  for (const char of Object.keys(occurences_a)) {
    if (!occurences_b.hasOwnProperty(char)) {
      count = 0;
      break;
    }

    count = Math.min(count, occurences_b[char] / occurences_a[char] >> 0);
  }

  return count;
}

const a = "ab";
const b = "abcbcba";
console.log(stringsConstruction(a, b));
