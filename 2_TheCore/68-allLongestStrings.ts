const findLongestString = (arr: string[]): number => {
  let maxLength: number = 0;

  arr.forEach((str) => {
    maxLength = Math.max(maxLength, str.length);
  });

  return maxLength;
};

const solution = (inputArray: string[]): string[] => {
  const maxLength: number = findLongestString(inputArray);
  return inputArray.filter((str) => str.length === maxLength);
};
