const makeArrayConsecutive2 = (arr: number[]): number => {
  let min: number = Number.MAX_SAFE_INTEGER,
    max: number = Number.MIN_SAFE_INTEGER,
    i: number;

  //Get the min and the max of the array
  for (i = 0; i < arr.length; i++) {
    min = Math.min(min, arr[i]);
    max = Math.max(max, arr[i]);
  }

  const arr_set: Set<Number> = new Set(arr);

  // Add every number between min and max to arr_set, since we use a Set, we can't add duplicates number
  for (i = min; i <= max; i++) {
    arr_set.add(i);
  }

  return arr_set.size - arr.length;
};