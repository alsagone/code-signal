function candles(candlesNumber: number, makeNew: number): number {
  let leftovers: number = 0,
    totalCandles: number = candlesNumber;

  while (candlesNumber > 0) {
    leftovers += candlesNumber;
    candlesNumber = (leftovers / makeNew) >> 0;
    leftovers = leftovers % makeNew;
    totalCandles += candlesNumber;
  }

  return totalCandles;
}

console.log(candles(5, 2));
