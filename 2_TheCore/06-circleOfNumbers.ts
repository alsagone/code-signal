const wrap = (n: number, minBound: number, maxBound: number): number => {
  const range = maxBound - minBound + 1;
  n = (n - minBound) % range;
  return n < 0 ? maxBound + 1 + n : minBound + n;
};

function circleOfNumbers(n: number, firstNumber: number): number {
  const arr: number[] = Array.from(Array(n).keys());
  const startingIndex: number = arr.indexOf(firstNumber);
  const index: number = wrap(
    startingIndex + arr.length / 2,
    0,
    arr.length - 1
  );
  return arr[index];
}