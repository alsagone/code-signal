function isTandemRepeat(inputString: string): boolean {
  if (inputString.length % 2 !== 0) {
    return false;
  }

  const firstHalf: string = inputString.slice(0, inputString.length / 2);
  return firstHalf.repeat(2) === inputString;
}

console.log(isTandemRepeat("tandemtandem"));
