function knapsackLight(
  value1: number,
  weight1: number,
  value2: number,
  weight2: number,
  maxW: number
): number {
  let maxValue: number = 0;
  const item_1: boolean = weight1 <= maxW;
  const item_2: boolean = weight2 <= maxW;

  // Can take both items
  if (weight1 + weight2 <= maxW) {
    maxValue = value1 + value2;
  }

  // If both items fit in the bag separately, choose the most valuable
  else if (item_1 && item_2) {
    maxValue = Math.max(value1, value2);
  }

  //Only item 1 can fit
  else if (item_1) {
    maxValue = value1;
  }

  //Only item 2 can fit
  else if (item_2) {
    maxValue = value2;
  }

  return maxValue;
}