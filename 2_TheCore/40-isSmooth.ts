const getMiddleElements = (arr: number[]): number[] => {
  const middleElements: number[] = [];

  if (arr.length % 2 !== 0) {
    middleElements.push(arr[(arr.length / 2) >> 0]);
  } else {
    const arrReverse: number[] = [...arr].reverse();

    for (let i = 0; i < arr.length; i++) {
      if (Math.abs(i - arrReverse.indexOf(arr[i])) === 1) {
        middleElements.push(arr[i]);
      }
    }
  }

  return middleElements;
};

function isSmooth(arr: number[]): boolean {
  if (arr.length === 0 || arr[0] !== arr[arr.length - 1]) {
    return false;
  }

  const middleElements: number[] = getMiddleElements(arr);

  return (
    middleElements.length > 0 &&
    middleElements.reduce((a, b) => a + b) === arr[0]
  );
}