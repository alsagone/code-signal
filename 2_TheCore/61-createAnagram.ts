function countOccurences(str: string): { [char: string]: number } {
  const occurences: { [char: string]: number } = {};

  for (const char of str) {
    if (occurences.hasOwnProperty(char)) {
      occurences[char] += 1;
    } else {
      occurences[char] = 1;
    }
  }

  return occurences;
}

const getCharAlphabetIndex = (char: string): number => {
  return char.toUpperCase().charCodeAt(0) - 65;
};

function createAnagram(s: string, t: string): number {
  const occurences: number[] = Array(26).fill(0);
  let count: number = 0;

  for (const char of s) {
    occurences[getCharAlphabetIndex(char)]++;
  }

  for (const char of t) {
    if (--occurences[getCharAlphabetIndex(char)] < 0) {
      count++;
    }
  }

  return count;
}

console.log(createAnagram("AABAA", "BBAAA"));
