function solution(picture: string[]): string[] {
  const borderLength: number = picture[0].length;
  const horizontalBorder: string = "*".repeat(borderLength + 2);
  const borderedPicture: string[] = [horizontalBorder];

  for (const str of picture) {
    borderedPicture.push(`*${str}*`);
  }

  borderedPicture.push(horizontalBorder);
  return borderedPicture;
}
