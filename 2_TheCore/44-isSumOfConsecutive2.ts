/* 
Create an empty queue ‘numQueue’.
Initialize two integers ‘count = 0’ and ‘sum = 0’. Use ‘count’ to count the number of ways to represent ‘n’ as the sum of consecutive numbers and ‘sum’ to store the sum of numbers in ‘numQueue’.
Initialize ‘x’ to ‘1’ and run a loop while ‘2 * x - 1 <= n’:
Insert ‘x’ at the end of ‘numQueue’.
‘sum += x’. Update ‘sum’ with ‘x’.
Run a loop while ‘sum > n’:
‘sum = sum - numQueue.front()’.
Remove the number at the front of ‘numQueue’.
If ‘sum’ is equal to ‘n’, then ‘count++’.
Return ‘count’ as the answer.
*/

function isSumOfConsecutive2(n: number): number {
  if (n < 3) {
    return 0;
  }

  let count: number = 0,
    sum: number = 0,
    i: number;
  const numQueue: number[] = [];

  for (i = 1; 2 * i - 1 <= n; i++) {
    numQueue.push(i);
    sum += i;

    while (sum > n) {
      sum -= numQueue.shift();
    }

    if (sum === n) {
      count++;
    }
  }
  return count;
}

console.log(isSumOfConsecutive2(1));
