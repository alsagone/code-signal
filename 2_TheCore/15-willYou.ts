function willYou(young: boolean, beautiful: boolean, loved: boolean): boolean {
  const statement_one: boolean = young && beautiful && !loved;
  const statement_two: boolean = loved && (!young || !beautiful);
  return statement_one || statement_two;
}
