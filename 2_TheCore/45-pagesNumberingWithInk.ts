const getNbDigits = (n: number): number => {
  return n.toString(10).length;
};

function pagesNumberingWithInk(
  current: number,
  totalDigits: number
): number {
  let pageNumber: number = current,
    done: boolean = false, nbDigits: number;

  while (!done) {
    nbDigits = getNbDigits(pageNumber);

    if (nbDigits > totalDigits) {
      pageNumber--;
      done = true;
    }

    else {
      totalDigits -= nbDigits;
      pageNumber++;
    }

  }

  return pageNumber;
}