function rangeBitCount(a: number, b: number): number {
  const arr: string[] = [];

  for (let i = a; i <= b; i++) {
    arr.push(...i.toString(2).split(""));
  }

  return arr.filter(x => x === '1').length;
}
