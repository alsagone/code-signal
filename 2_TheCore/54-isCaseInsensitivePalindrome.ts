const isPalindrome = (str: string): boolean => {
  return str === str.split("").reverse().join("");
}

function isCaseInsensitivePalindrome(inputString: string): boolean {
  return isPalindrome(inputString.toLowerCase());
}
