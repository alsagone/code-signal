/*
Let's say that number a feels comfortable with number b if a ≠ b and
b lies in the segment [a - s(a), a + s(a)], where s(x) is the sum of x's digits.

How many pairs (a, b) are there, such that a < b, both a and b lie on the segment [l, r],
and each number feels comfortable with the other
(so a feels comfortable with b and b feels comfortable with a)?

For l = 10 and r = 12, the output should be
comfortableNumbers(l, r) = 2.

Here are all values of s(x) to consider:

s(10) = 1, so 10 is comfortable with 9 and 11;
s(11) = 2, so 11 is comfortable with 9, 10, 12 and 13;
s(12) = 3, so 12 is comfortable with 9, 10, 11, 13, 14 and 15.
Thus, there are 2 pairs of numbers comfortable
with each other within the segment [10; 12]: (10, 11) and (11, 12).
*/

const getSumOfDigits = (n: number): number => {
  return n
    .toString(10)
    .split("")
    .map(Number)
    .reduce((a, b) => a + b);
};

const isBetween = (x: number, min: number, max: number): boolean => {
  return min <= x && x <= max;
};

const isComfortable = (a: number, b: number): boolean => {
  if (a === b) {
    return false;
  }

  const a_sumDigits: number = getSumOfDigits(a);
  return isBetween(b, a - a_sumDigits, a + a_sumDigits);
};

const areBothComfortable = (a: number, b: number): boolean => {
  return isComfortable(a, b) && isComfortable(b, a);
};

const countComfortableNumbers = (
  x: number,
  l: number,
  r: number
): number => {
  const x_sumDigits: number = getSumOfDigits(x);
  const start: number = Math.max(l, x - x_sumDigits);
  const stop: number = Math.min(r, x + x_sumDigits);
  let count: number = 0;

  for (let i = start; i <= stop; i++) {
    if (x < i && areBothComfortable(x, i)) {
      count++;
    }
  }

  return count;
};

function comfortableNumbers(l: number, r: number): number {
  let a: number,
    count: number = 0;

  for (a = l; a <= r; a++) {
    count += countComfortableNumbers(a, l, r);
  }

  return count;
}

console.log(countComfortableNumbers(12, 10, 12));
console.log(comfortableNumbers(10, 12));
