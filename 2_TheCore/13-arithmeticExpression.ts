function arithmeticExpression(a: number, b: number, c: number) {
  let resultValue: number;

  for (const sign of ["+", "-", "*", "/"]) {
    switch (sign) {
      case "+":
        resultValue = a + b;
        break;
      case "-":
        resultValue = a - b;
        break;
      case "*":
        resultValue = a * b;
        break;
      case "/":
        resultValue = a / b;
        break;
      default:
        break;
    }

    if (resultValue === c) {
      return true;
    }
  }

  return false;
}
