function maxMultiple(divisor: number, bound: number): number {
  let i: number = 0;

  while (divisor * i <= bound) {
    i++;
  }

  return divisor * (i - 1);
}

console.log(maxMultiple(3, 10));
