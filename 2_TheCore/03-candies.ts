function candies(n: number, m: number): number {
  let count: number = 0;

  while (count * n <= m) {
    count++;
  }

  return (count - 1) * n;
}

console.log(candies(3, 10))
