const gcd = (a: number, b: number): number => {
  if (a === 0) {
    return b;
  }

  if (b === 0) {
    return a;
  }

  while (a !== b) {
    if (a > b) {
      a -= b;
    } else {
      b -= a;
    }
  }

  return a;
};

function countBlackCells(n: number, m: number): number {
  return m + n + gcd(n, m) - 2;
}